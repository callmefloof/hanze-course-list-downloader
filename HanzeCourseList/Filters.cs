﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HanzeCourseList
{
    enum FilterByType { Programme, Exchange, Minor, Course };

    public class Filters
    {
        public string? SearchCriteria { get; set; }
        public string? SelectedSchoolYear { get; set; }
        public string? FilterByType { get; set; }

        public Filters(string searchCritria, string year, int filterByType)
        {
            FilterByType type = (FilterByType)filterByType;
            SearchCriteria = searchCritria;
            SelectedSchoolYear = year;
            FilterByType = type.ToString();
        }

        public Filters()
        {

        }
    }
}
