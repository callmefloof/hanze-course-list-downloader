﻿using HtmlAgilityPack;
using PdfSharp.Pdf;
using PdfSharp.Pdf.Advanced;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace HanzeCourseList
{

    public class Search
    {
        private static HttpClient client = new HttpClient();

        public static async Task<List<string>> getCourseUrls(string? link)
        {
            var response = await client.GetStringAsync(link);
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(response);
            var Links = htmlDocument.DocumentNode.Descendants("a")
                .Where(x => x.GetAttributeValue("href", "").Contains("course")).ToList();
            List<string> urls = new List<string>();
            foreach (var Link in Links)
            {

                urls.Add(Link.GetAttributeValue("href", ""));
            }
            return urls;
        }
        public static async Task<List<string>> GetPrintUrls(List<string> urls)
        {

            string UrlBase = "https://catalogue.hanze.nl";

            List<string> courseLinks = new List<string>();
            foreach (var u in urls)
            {
                var secondResponse = await client.GetStringAsync(UrlBase + u);
                courseLinks.Add(secondResponse);
            }

            List<string> printLinks = new List<string>();
            HtmlDocument htmlDocument = new HtmlDocument();
            foreach (var courseLink in courseLinks)
            {
                htmlDocument.LoadHtml(courseLink);
                var Links = htmlDocument.DocumentNode.Descendants("a")
                    .Where(x => x.GetAttributeValue("href", "").Contains("print")).ToList();

                foreach(var pLink in Links)
                {
                    printLinks.Add(pLink.GetAttributeValue("href", ""));
                }

            }

            return printLinks;

        }

        public static async Task<string> GetPrintUrl(string? link)
        {
            //string UrlBase = "https://catalogue.hanze.nl";

            var response = await client.GetStringAsync(link);
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(response);
            var Links = htmlDocument.DocumentNode.Descendants("a")
                .Where(x => x.GetAttributeValue("href", "").Contains("print")).Where(x=> x.GetAttributeValue("class", "").Contains("nav-link")).ToList();
            List<string> printLinks = new List<string>();
            foreach (var Link in Links)
            {

                printLinks.Add(Link.GetAttributeValue("href", ""));
            }
            
            return printLinks.First();
        }


        public static async Task<List<byte[]>> CollectFiles(string courseOverviewLink, List<string> courseLinks)
        {
            string UrlBase = "https://catalogue.hanze.nl";

            List<byte[]> result = new List<byte[]>();


            var overviewDocData = await client.GetByteArrayAsync(UrlBase + courseOverviewLink);
            result.Add(overviewDocData);

            foreach (string courseLink in courseLinks)
            {
                var uri = UrlBase + courseLink;
                result.Add(await client.GetByteArrayAsync(uri));
            }

            return result;
        }


        public static void SavePDFs(List<byte[]> PDFs, byte[] mergedPDF)
        {
            string currentpath = Directory.GetCurrentDirectory();
            if (!Directory.Exists(Path.Combine(currentpath, "PDFs")))
            {
                Directory.CreateDirectory(Path.Combine(currentpath, "PDFs"));
            }
            string pdfFolder = Path.Combine(currentpath, "PDFs");
            if (!Directory.Exists(Path.Combine(pdfFolder, "Individual_PDFs")))
            {
                Directory.CreateDirectory(Path.Combine(pdfFolder, "Individual_PDFs"));
            }
            string individualPDFsPath = Path.Combine(pdfFolder, "Individual_PDFs");

            File.WriteAllBytes(pdfFolder + @"\merged.pdf", mergedPDF);
            int i = 0;
            foreach(var pdf in PDFs)
            {
                File.WriteAllBytes(individualPDFsPath + $@"\file-{i}.pdf", pdf);
                i++;
            }

        }
        public static byte[] CombinePDFs(List<byte[]> srcPDFs)
        {

            System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
            using (var ms = new MemoryStream())
            {
                using (var resultPDF = new PdfDocument(ms))
                {
                    foreach (var pdf in srcPDFs)
                    {
                        using (var src = new MemoryStream(pdf))
                        {
                            using (var srcPDF = PdfReader.Open(src, PdfDocumentOpenMode.Import))
                            {
                                for (var i = 0; i < srcPDF.PageCount; i++)
                                {
                                    resultPDF.AddPage(srcPDF.Pages[i]);
                                }
                            }
                        }
                    }
                    resultPDF.Save(ms);
                    return ms.ToArray();
                }
            }
        }

    }
}
