﻿// See https://aka.ms/new-console-template for more information
using HanzeCourseList;
using HtmlAgilityPack;
using RestSharp;
using System.Net;
using System.Text;
using static System.Net.WebRequestMethods;

Console.WriteLine();
Console.WriteLine();
Console.WriteLine();


Console.WriteLine("Welcome.\n Keep in mind there is not error checking.");

Console.WriteLine("Please give the webpage of the Programme: ");
string? link = Console.ReadLine();
Console.WriteLine("Getting Course Urls...");
var courseUrls = await Search.getCourseUrls(link);
var coursePrintUrls = await Search.GetPrintUrls(courseUrls);
var courseOverViewUrl = await Search.GetPrintUrl(link);
Console.WriteLine("Downloading...");
var PDFS = await Search.CollectFiles(courseOverViewUrl, coursePrintUrls);
Console.WriteLine("Merging...");
var merged = Search.CombinePDFs(PDFS);
Console.WriteLine("Saving...");
Search.SavePDFs(PDFS, merged);
Console.WriteLine("Done!");



