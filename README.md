### To run:
- Go to the hanze ECTS Course Calogue website: https://catalogue.hanze.nl/en/
- Find the programme you are in.
- Copy the url of the webpage
- Run the .exe (or the binary of the relevant OS)
    - Note: Requires the .Net 6 (Core) Runtime
- Paste the link and press ENTER
- It will take approximately 4 minutes to do depending on your download speed
    - Downloads are not done parallel, so it may take a while.
- Once the message "Done!" appears, you should a folder called "PDFs".
    - In there you will find the resulting PDFs

#### This application was built using:
- PdfSharp: http://www.pdfsharp.net/
- RestSharp (Not Utilized): https://restsharp.dev/
- Html Agility Pack (HAP): https://html-agility-pack.net/
- System.Text.Encoding.CodePages: https://www.nuget.org/packages/System.Text.Encoding.CodePages/

